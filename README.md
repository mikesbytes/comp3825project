## COMP 3825 Networking Course Project ##

*A note to the grader: Unfortunately I ended up with much less time to burn over the last several
weeks than I would have liked so I completely reworked the project from my original design so
I could finish everything on time. It is also far easier to set up.*

### Usage instructions and notes: ###

run `server.py` with python3

in a separate terminal, run `client.py`. It will prompt for a username.

After that it will give you a list of currently connected users.

You can have one recipient selected, either a user or a channel. If you try to send a message
without a selected recipient, you will receive an error.

### Notes about channels

You can also select a channel as a recipient without joining it. Having a channel as your recipient
will send your mesage to all other users joined to that channel.

Joining a nonexistent channel will create it.

### Client commands

In the client you have several commands available

- `/users`: lists users
- `/channels`: lists channels
- `/rec <username|#channel>`: change recipient *(e.g. `/rec bob`, `/rec #movies`)
- `/join <#channel>`: join a channel to receive messages sent to it
- `/leave <#channel>`: leave a channel
- `/quit` or `.exit`: close the client and disconnect


