#!/usr/bin/python3
import socket
from _thread import *

username = input("Enter a username, AUTO for auto username: ")

s = socket.socket()
host = socket.gethostname()
port = 3825

def threaded_client(connection):
   while True:
      data = connection.recv(2048)
      if not data:
         break

      print(data.decode("utf-8"))
    
s.connect((host, port))
s.sendall(username.encode("utf-8"))
start_new_thread(threaded_client, (s, )) 

while True:
   msg = input()
   if msg == "/quit" or msg == ".exit":
      break

   s.sendall(msg.encode("utf-8"))

s.close()
