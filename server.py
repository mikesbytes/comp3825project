#!/usr/bin/python3

import socket
import os
from _thread import *

users = {}
channels = {}
auto_uid = 0

sv = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

host = socket.gethostname()

port = 3825

# bind to the port
sv.bind((host, port))

# queue up to 5 requests
sv.listen(5)

def validate_recipient(recipient):
   if recipient in users:
      return True
   elif recipient.startswith("#"):
      if not recipient in channels:
         channels[recipient] = []
      return True

def currently_connected_msg():
   msg = "Currently connected users: "
   for user in users:
      msg += "\n" + user
   return msg

def channels_msg():
   msg = "Channels: "
   for user in channels:
      msg += "\n" + user
   return msg

def send_to(sender, recipient, msg):
   if recipient in users:
      msg = "{}: {}".format(sender, msg)
      users[recipient].sendall(msg.encode("utf-8"))
   elif recipient in channels:
      msg = "({}) {}: {}".format(recipient, sender, msg)
      for username in channels[recipient]:
         users[username].sendall(msg.encode("utf-8"))

def threaded_client(connection):
   #initial setup

   recipient = ""

   # get username
   username = connection.recv(2048)
   if not username:
      connection.close()
      return

   username = username.decode("utf-8").strip()
   if username == "AUTO" or username in users:
      gen_name = len(users)
      while str(gen_name) + "_auto" in users:
         gen_name += 1
      username = str(gen_name) + "_auto"
   users[username] = connection
   connection.sendall(currently_connected_msg().encode('utf-8'))

   while True:
      data = connection.recv(2048)
      if not data:
         break
      data = data.decode('utf-8')
      print("From {}: {}".format(username,data))

      msg = None
      if data == "/users": # list users
         msg = currently_connected_msg()

      elif data == "/channels": # list channels
         msg = channels_msg()

      elif data.startswith("/rec"): # select a recipient
         recipient_msg = data[data.find(' '):].strip()
         print("{} switching recipient to {}".format(username, recipient_msg))
         if validate_recipient(recipient_msg):
            recipient = recipient_msg
            msg = "Changed recipient to {}".format(recipient)
         else:
            msg = "Could not locate recipient"

      elif data.startswith("/join"): # join a channel
         channel = data[data.find(' '):].strip()
         print("{} joining channel {}".format(username, channel))
         if not channel in channels:
            channels[channel] = []
         channels[channel].append(username)

      elif data.startswith("/leave"): # leave a channel
         channel = data[data.find(' '):].strip()
         print("{} leaving channel {}".format(username, channel))
         if not channel in channels:
            msg = "Not a valid channel. View channels with /channels"
         elif not username in channels[channel]:
            msg = "You are not in {}".format(channel)
         else:
            channels[channel].remove(username)
            if len(channels[channel]) == 0:
               channels.pop(channel)
         
      elif not data.startswith("/"): # send message
         if validate_recipient(recipient):
            send_to(username, recipient, data)
         else:
            msg = "Use /rec <username|#channel> to set a valid recipient"

      else:
         msg = "Input not recognized"

      if msg:
         print(msg)
         connection.sendall(msg.encode("utf-8"))

   print("{} disconnected".format(username))
   users.pop(username)
   for key in list(channels.keys()):
      if username in channels[key]:
         channels[key].remove(username)
         if len(channels[key]) == 0:
            channels.pop(key)
   connection.close()
   
while True:
   # establish a connection
   client,addr = sv.accept()      

   print("Connected to %s" % str(addr))
   start_new_thread(threaded_client, (client, )) 

sv.close()
